output "cross_account_access_role_arn" {
  description = "CodePipelineからのクロスアカウントアクセスに使用するRoleのARN"
  value       = module.cd_pipeline_backend_trigger_notifier.cross_account_access_role_arn
}
