data "terraform_remote_state" "staging_cacheable_frontend" {
  backend = "s3"

  config = {
    bucket         = "tanaka-staging-terraform-tfstate"
    key            = "cacheable_frontend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "tanaka_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/TanakaStagingTerraformBackendAccessRole"
  }
}
